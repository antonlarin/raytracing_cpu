#ifndef OBJECTS_HPP
#define OBJECTS_HPP

#include "DataEntities.hpp"

class TraceObject
{
public:
  virtual ~TraceObject() {}
  virtual IntersectData computeIntersect(const Ray &r) = 0;

  vec3 ambient;
  vec3 diffuse;
  vec3 specular;
  float gloss;
  float refl;
  float transp;
};

class Sphere : public TraceObject
{
public:
  virtual ~Sphere() {}
  virtual IntersectData computeIntersect(const Ray &r);

  vec3 center;
  float rad;
};

class Polygon : public TraceObject
{
public:
  virtual ~Polygon() {}
  virtual IntersectData computeIntersect(const Ray &r);

  std::vector<vec3> vertices;
  float A, B, C, D;
};

class Box : public TraceObject
{
public:
  virtual ~Box() {}
  virtual IntersectData computeIntersect(const Ray &r);

  vec3 min;
  vec3 max;
};

#endif // OBJECTS_HPP
