#ifndef IMAGEDISPLAYER_HPP
#define IMAGEDISPLAYER_HPP

#include <QWidget>
#include <QPainter>

class ImageDisplayer : public QWidget
{
  Q_OBJECT
public:
  ImageDisplayer(QWidget*& parent) : QWidget(parent) {}

  void setImage(QImage &img)
  {
    image = img;
  }

protected:
  void paintEvent(QPaintEvent *e)
  {
    QPainter p(this);
    QRectF source(0.0, 0.0, image.width(), image.height());
    QRectF target((this->width() - image.width()) / 2.0f,
                  (this->height() - image.height()) / 2.0f,
                  image.width(), image.height());
    p.drawImage(target, image, source);
  }

private:
  QImage image;
};

#endif // IMAGEDISPLAYER_HPP
