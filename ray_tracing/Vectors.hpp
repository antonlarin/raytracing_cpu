#ifndef VECTORS_HPP
#define VECTORS_HPP

#include <cmath>

// 3-dimensional float vector

class vec3
{
public:
  vec3(): x(0.0f), y(0.0f), z(0.0f) {}
  vec3(const float &x, const float &y, const float &z): x(x), y(y), z(z) {}
  vec3(const vec3& v): x(v.x), y(v.y), z(v.z) {}

  void set(const float &x, const float &y, const float &z)
  {
    this->x = x;
    this->y = y;
    this->z = z;
  }

  float normSquared() const
  {
    return x*x + y*y + z*z;
  }

  float norm() const
  {
    return sqrt(normSquared());
  }

  vec3& operator=(const vec3& v)
  {
    x = v.x;
    y = v.y;
    z = v.z;
    return *this;
  }

  vec3& operator+=(const vec3& v)
  {
    x += v.x;
    y += v.y;
    z += v.z;
    return *this;
  }

  vec3& operator-=(const vec3& v)
  {
    x -= v.x;
    y -= v.y;
    z -= v.z;
    return *this;
  }

  vec3& operator*=(const vec3& v)
  {
    x *= v.x;
    y *= v.y;
    z *= v.z;
    return *this;
  }

  vec3& operator*=(const float& m)
  {
    x *= m;
    y *= m;
    z *= m;
    return *this;
  }

  float x, y, z;
};

vec3 operator+(const vec3 &v1, const vec3 &v2);
vec3 operator-(const vec3 &v1, const vec3 &v2);

vec3 operator*(const vec3 &v1, const vec3 &v2);
vec3 operator*(const float &m, const vec3 &v);
vec3 operator*(const vec3 &v, const float &m);

vec3 operator/(const float &m, const vec3 &v);
vec3 operator/(const vec3 &v, const float &m);

vec3 normalize(const vec3 &v);
float dot(const vec3& v1, const vec3& v2);
vec3 cross(const vec3& v1, const vec3& v2);

#endif // VECTORS_HPP
