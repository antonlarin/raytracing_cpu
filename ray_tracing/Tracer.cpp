#include <fstream>
#include <cfloat>
#include <cmath>
#include <iostream>

#include <QColor>

#include "Tracer.hpp"

Tracer::Tracer(std::string filename)
{
  this->filename = filename;
  scene = new TraceObject*[MAX_OBJECTS_NUM];
  numOfObjects = 0;
  lightSources = new LightSource*[MAX_OBJECTS_NUM];
  numOfLightSources = 0;
}

Tracer::~Tracer()
{
  for (unsigned int i = 0; i < numOfObjects; i++)
    delete scene[i];
  delete[] scene;
  for (unsigned int i = 0; i < numOfLightSources; i++)
    delete lightSources[i];
  delete[] lightSources;
}

QImage Tracer::formImage()
{
  QImage resultImage(frameSizeX, frameSizeY, QImage::Format_RGB32);

  vec3 imagePlaneXAxis = cross(camera.dir, camera.normal);
  vec3 imagePlaneYAxis = camera.normal;
  vec3 imagePlaneOrigin = camera.pos + camera.dir * camera.dist -
      imagePlaneXAxis * (camera.imagePlaneWidth / 2.0f) -
      imagePlaneYAxis * (camera.imagePlaneHeight / 2.0f);

  float dx = camera.imagePlaneWidth / frameSizeX;
  float dy = camera.imagePlaneHeight / frameSizeY;

  #pragma omp parallel for
  for (unsigned int i = 0; i < frameSizeY; i++)
  for (unsigned int j = 0; j < frameSizeX; j++)
  {
    float aaDx = dx / ANTIALIASING_ZONE;
    float aaDy = dy / ANTIALIASING_ZONE;
    vec3 aaLuminosity;

    for (unsigned int k = 0; k < ANTIALIASING_ZONE; k++)
    for (unsigned int l = 0; l < ANTIALIASING_ZONE; l++)
    {
      vec3 currentImagePlanePoint = imagePlaneOrigin +
          imagePlaneXAxis * (dx * j + aaDx * (0.5f + k)) +
          imagePlaneYAxis * (dy * i + aaDy * (0.5f + l));
      vec3 primaryRayDir = currentImagePlanePoint - camera.pos;

      Ray primary(camera.pos, primaryRayDir, RayType::RAY_PRIMARY);
      aaLuminosity += shootRay(primary, 1);

    }
    aaLuminosity *= 255.0f / 9.0f;
    QRgb value = qRgb((int)aaLuminosity.x,
                      (int)aaLuminosity.y,
                      (int)aaLuminosity.z);

    QRgb* line = (QRgb*)resultImage.scanLine(frameSizeY - i - 1);
    line[j] = value;
  }
  return resultImage;
}

vec3 Tracer::shootRay(Ray r, unsigned int level)
{
  // Determine intersection point closest to the origin of the ray
  vec3 interPointDist(FLT_MAX, 0, 0);
  unsigned int minDistObj = 0;
  IntersectData id;
  bool intersectionExists = false;

  for (unsigned int i = 0; i < numOfObjects; i++)
  {
    IntersectData currentID = scene[i]->computeIntersect(r);
    if (currentID.exists)
    {
      intersectionExists = true;
      vec3 currentDist = currentID.point - r.orig;
      if (currentDist.norm() < interPointDist.norm())
      {
        minDistObj = i;
        interPointDist = currentDist;
        id = currentID;
      }
    }
  }

  if (!intersectionExists)
    return ambientColor;

  // Local illumination (Phong)
  vec3 lLocal = ambientColor * scene[minDistObj]->ambient;
  for (unsigned int i = 0; i < numOfLightSources; i++)
  {
    vec3 toLightDir = normalize(lightSources[i]->pos - id.point);
    Ray rayToLight(id.point, toLightDir, RayType::RAY_SHADOW);
    bool isInShadow = shootShadowRay(rayToLight);
    if (!isInShadow)
    {
      float diffuse_coeff = std::max(0.0f, dot(id.normal, toLightDir));
      vec3 ref = 2.0f * id.normal * dot(id.normal, toLightDir) - toLightDir;
      vec3 v = -1.0f * r.dir;
      float specular_coeff =
          pow(std::max(0.0f, dot(ref, v)), scene[minDistObj]->gloss);
      lLocal += lightSources[i]->color * (scene[minDistObj]->diffuse *
                                        diffuse_coeff +
                                        scene[minDistObj]->specular *
                                        specular_coeff);
    }
  }

  // No transparency or reflections on the deepest level
  if (level == MAX_TRACE_LEVEL)
    return lLocal;

  // Reflection component
  vec3 lRefl;
  if (scene[minDistObj]->refl > 0.0)
  {
    vec3 reflRayDir = -2.0f * id.normal * dot(id.normal, r.dir) + r.dir;
    Ray reflRay(id.point, reflRayDir, RayType::RAY_REFLECT);
    lRefl = scene[minDistObj]->refl * shootRay(reflRay, level + 1);
  }

  vec3 result = lLocal + lRefl;
  if (result.x > 1.0f)
    result.x = 1.0f;
  if (result.y > 1.0f)
    result.y = 1.0f;
  if (result.z > 1.0f)
    result.z = 1.0f;

  return result;
}

bool Tracer::shootShadowRay(Ray &r)
{
  for (unsigned int i = 0; i < numOfObjects; i++)
  {
    IntersectData id = scene[i]->computeIntersect(r);
    if (id.exists) {
      return true;
    }
  }
  return false;
}

Sphere* Tracer::loadSphere(std::ifstream& in)
{
  float cx, cy, cz;
  Sphere *s = new Sphere();

  in >> cx >> cy >> cz;
  s->center.set(cx, cy, cz);
  in >> s->rad;

  loadMaterialProperties(in, s);
  return s;
}

Polygon* Tracer::loadPolygon(std::ifstream &in)
{
  float x, y, z;
  Polygon *p = new Polygon();
  in >> p->A >> p->B >> p->C >> p->D;

  for (unsigned int i = 0; i < 4; i++)
  {
    in >> x >> y >> z;
    p->vertices.push_back(vec3(x, y, z));
  }

  loadMaterialProperties(in, p);
  return p;
}

Box *Tracer::loadBox(std::ifstream& in)
{
  float x, y, z;
  Box *b = new Box();

  in >> x >> y >> z;
  b->min.set(x, y, z);
  in >> x >> y >> z;
  b->max.set(x, y, z);

  loadMaterialProperties(in, b);
  return b;
}

void Tracer::loadMaterialProperties(std::ifstream& in, TraceObject* o)
{
  float ar, ag, ab; // ambient coefficients
  float dr, dg, db; // diffuse coefficients
  float sr, sg, sb; // specular coefficients

  in >> ar >> ag >> ab;
  o->ambient.set(ar, ag, ab);

  in >> dr >> dg >> db;
  o->diffuse.set(dr, dg, db);

  in >> sr >> sg >> sb;
  o->specular.set(sr, sg, sb);

  // glossiness, reflection and transparency factors
  in >> o->gloss >> o->refl >> o->transp;
}

void Tracer::loadData()
{
  float x, y, z;
  vec3 lightOrig;
  vec3 lightColor;

  LightSource *ls;
  Sphere *s;
  Box *b;
  Polygon *p;
  std::ifstream srcFile(filename.c_str());
  while (srcFile.good())
  {
    char id = srcFile.get();
    switch (id)
    {
    case 'F': // frame size
      srcFile >> frameSizeX >> frameSizeY;
      break;
    case 'C': // camera position
      float dirx, diry, dirz;
      float normx, normy, normz;
      float dist, ipw, iph;
      srcFile >> x >> y >> z;
      srcFile >> dirx >> diry >> dirz;
      srcFile >> normx >> normy >> normz;
      srcFile >> dist >> ipw >> iph;

      camera.pos = vec3(x, y, z);
      camera.dir = vec3(dirx, diry, dirz);
      camera.normal = vec3(normx, normy, normz);
      camera.dist = dist;
      camera.imagePlaneWidth = ipw;
      camera.imagePlaneHeight = iph;
      break;
    case 'L': // light source
      float lr, lg, lb;
      srcFile >> x >> y >> z;
      srcFile >> lr >> lg >> lb;
      lightOrig.set(x, y, z);
      lightColor.set(lr, lg, lb);
      ls = new LightSource(lightOrig, lightColor);
      lightSources[numOfLightSources++] = ls;
      break;
    case 'S': // sphere object
      s = loadSphere(srcFile);
      scene[numOfObjects] = s;
      numOfObjects += 1;
      break;
    case 'P': // polygon object
      p = loadPolygon(srcFile);
      scene[numOfObjects] = p;
      numOfObjects += 1;
      break;
    case 'B': // box object
      b = loadBox(srcFile);
      scene[numOfObjects] = b;
      numOfObjects += 1;
      break;
    case 'A': // ambient lighting color
      float ar, ag, ab;
      srcFile >> ar >> ag >> ab;
      ambientColor.set(ar, ag, ab);
      break;
    }
    while (srcFile.good() &&
           (srcFile.peek() == ' ' || srcFile.peek() == '\n'))
      srcFile.get();
  }

  srcFile.close();
}
