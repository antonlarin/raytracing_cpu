#ifndef RAY_HPP
#define RAY_HPP

#include "Vectors.hpp"

enum class RayType {
  RAY_PRIMARY,
  RAY_SHADOW,
  RAY_REFLECT,
  RAY_TRANSMIT
};

class Ray
{
public:
  Ray(vec3 &orig, vec3 &dir, RayType type) :
    type(type), orig(orig), dir(normalize(dir)) {}

  RayType type;
  vec3 orig;
  vec3 dir;
};

class LightSource
{
public:
  LightSource() {}
  LightSource(vec3 &pos, vec3 &color): pos(pos), color(color) {}

  void setData(vec3 &pos, vec3 &color)
  {
    this->pos = pos;
    this->color = color;
  }

  vec3 pos;
  vec3 color;
};

class Camera
{
public:
  Camera(): pos(), dir(), normal(), dist(0.0f), imagePlaneWidth(0.0f),
    imagePlaneHeight(0.0f) {}

  Camera(vec3 &pos, vec3 &dir, vec3 &normal,
         float &dist, float &ipw, float &iph):
    pos(pos), dir(normalize(dir)), normal(normalize(normal)),
    dist(dist), imagePlaneWidth(ipw), imagePlaneHeight(iph)
  {}

  vec3 pos;
  vec3 dir;
  vec3 normal;
  float dist;
  float imagePlaneWidth;
  float imagePlaneHeight;
};

class IntersectData
{
public:
  bool exists;
  vec3 point;
  vec3 normal;
};

#endif // RAY_HPP
