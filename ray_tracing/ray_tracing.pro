#-------------------------------------------------
#
# Project created by QtCreator 2014-05-21T20:07:46
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ray_tracing
TEMPLATE = app

CONFIG += c++11

QMAKE_CXXFLAGS += -fopenmp -Wall -Wextra

LIBS += -fopenmp

SOURCES += main.cpp\
        mainwindow.cpp \
    Tracer.cpp \
    Objects.cpp \
    Vectors.cpp

HEADERS  += mainwindow.hpp \
    Tracer.hpp \
    DataEntities.hpp \
    Objects.hpp \
    ImageDisplayer.hpp \
    Vectors.hpp

FORMS    += mainwindow.ui
