#include "Vectors.hpp"

vec3 operator+(const vec3 &v1, const vec3 &v2)
{
  return vec3(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
}

vec3 operator-(const vec3 &v1, const vec3 &v2)
{
  return vec3(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
}

vec3 operator*(const vec3 &v1, const vec3 &v2)
{
  return vec3(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z);
}

vec3 operator*(const float &m, const vec3 &v)
{
  return vec3(v.x * m, v.y * m, v.z * m);
}

vec3 operator*(const vec3 &v, const float &m)
{
  return vec3(v.x * m, v.y * m, v.z * m);
}

vec3 operator/(const float &m, const vec3 &v)
{
  return vec3(v.x / m, v.y / m, v.z / m);
}

vec3 operator/(const vec3 &v, const float &m)
{
  return vec3(v.x / m, v.y / m, v.z / m);
}

vec3 normalize(const vec3 &v)
{
  return v / v.norm();
}

float dot(const vec3& v1, const vec3& v2)
{
  return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}

vec3 cross(const vec3& v1, const vec3& v2)
{
  return vec3(v1.y * v2.z - v1.z * v2.y,
              v1.z * v2.x - v1.x * v2.z,
              v1.x * v2.y - v1.y * v2.x);
}
