#include "mainwindow.hpp"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  tracer = nullptr;

  connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(runTracing()));
}

MainWindow::~MainWindow()
{
  if (tracer != nullptr)
    delete tracer;

  delete ui;
}

void MainWindow::runTracing()
{
  if (tracer != nullptr)
    delete tracer;

  tracer = new Tracer(ui->lineEdit->text().toStdString());
  tracer->loadData();
  QImage result = tracer->formImage();
  ui->widget->setImage(result);
  ui->widget->repaint();
}
