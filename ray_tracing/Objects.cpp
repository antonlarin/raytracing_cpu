#include <cmath>
#include <cfloat>

#include <vector>

#include "Objects.hpp"

IntersectData Sphere::computeIntersect(const Ray &r)
{
  IntersectData result;
  vec3 v = r.orig - center;
  float B = 2.0f * dot(r.dir, v);
  float C = v.normSquared() - rad * rad;

  float eps = 0.0001; // do not detect intersections closer than this

  float discr = B*B - 4.0f*C;
  if (discr < 0.0f)
  {
    result.exists = false;
    return result;
  }
  float t1 = -0.5f * (B + sqrt(discr));
  float t2 = -0.5f * (B - sqrt(discr));

  if (t1 < eps && t2 < eps)
  {
    result.exists = false;
    return result;
  }

  result.exists = true;
  float t;
  if (t1 < eps)
    t = t2;
  else if (t2 < eps)
    t = t1;
  else if (t1 < t2)
    t = t1;
  else
    t = t2;

  result.point = r.orig + t * r.dir;
  result.normal = (result.point - center) / rad;

  return result;
}

IntersectData Polygon::computeIntersect(const Ray &r)
{
  IntersectData result;
  float eps = 0.0001; // do not detect intersections closer than this

  // First find intersection with plane
  vec3 abc = vec3(A, B, C);
  vec3 Pn = normalize(abc);

  float Vd = dot(r.dir, abc);
  float normalCoeff = 1;

  if (Vd == 0)
  {
    result.exists = false;
    return result;
  }
  else if (Vd > 0)
    normalCoeff = -1;

  float V0 = -(dot(abc, r.orig) + D);
  float t = V0 / Vd;
  if (t < eps)
  {
    result.exists = false;
    return result;
  }

  result.point = r.orig + t * r.dir;
  result.normal = Pn * normalCoeff;
  result.exists = true;

  // Check if intersection point is in polygon
  //  Project on dominant plane
  vec3 Pnabs = vec3(fabs(Pn.x), fabs(Pn.y), fabs(Pn.z));

  vec3 reducedInterPoint;
  std::vector<vec3> reducedVertices;
  if (Pnabs.x > Pnabs.y)
    if (Pnabs.x > Pnabs.z)
    {
      reducedInterPoint = vec3(result.point.y, result.point.z, 0.0f);
      for (auto i = vertices.begin(); i != vertices.end(); i += 1)
        reducedVertices.push_back(vec3(i->y, i->z, 0.0f));
    }
    else
    {
      reducedInterPoint = vec3(result.point.x, result.point.y, 0.0f);
      for (auto i = vertices.begin(); i != vertices.end(); i += 1)
        reducedVertices.push_back(vec3(i->x, i->y, 0.0f));
    }
  else
    if (Pnabs.z > Pnabs.y)
    {
      reducedInterPoint = vec3(result.point.x, result.point.y, 0.0f);
      for (auto i = vertices.begin(); i != vertices.end(); i += 1)
        reducedVertices.push_back(vec3(i->x, i->y, 0.0f));
    }
    else
    {
      reducedInterPoint = vec3(result.point.x, result.point.z, 0.0f);
      for (auto i = vertices.begin(); i != vertices.end(); i += 1)
        reducedVertices.push_back(vec3(i->x, i->z, 0.0f));
    }

  // Shift origin to reduced intersection point
  for (auto i = reducedVertices.begin(); i != reducedVertices.end(); i += 1)
    *i -= reducedInterPoint;

  // Count crossings of probe ray and edges of polygon
  unsigned int numCrossings = 0;
  int signHolder = (reducedVertices[0].y < 0) ? -1 : 1;
  for (unsigned int i = 0; i < reducedVertices.size(); i += 1)
  {
    unsigned int j = (i + 1) % reducedVertices.size();
    int nextSignHolder = (reducedVertices[j].y < 0) ? -1 : 1;
    if (signHolder != nextSignHolder)
    {
      if (reducedVertices[i].x > 0 && reducedVertices[j].x > 0)
        numCrossings += 1;
      else
      {
        float reducedInterX = reducedVertices[i].x - reducedVertices[i].y *
          (reducedVertices[j].x - reducedVertices[i].x) /
          (reducedVertices[j].y - reducedVertices[i].y);
        if (reducedInterX > 0)
          numCrossings += 1;
      }
    }
    signHolder = nextSignHolder;
  }
  if (numCrossings % 2 == 0)
    result.exists = false;

  return result;
}

IntersectData Box::computeIntersect(const Ray &r)
{
  IntersectData result;
  float eps = 0.0001f; // do not detect intersections closer than this

  // X planes
  float Tnear = FLT_MIN;
  float Tfar = FLT_MAX;

  if (r.dir.x == 0)
  {
    if (r.orig.x < min.x || r.orig.x > max.x)
    {
      result.exists = false;
      return result;
    }
  }
  else
  {
    float T1 = (min.x - r.orig.x) / r.dir.x;
    float T2 = (max.x - r.orig.x) / r.dir.x;

    if (T1 > T2) std::swap(T1, T2);
    if (T1 > Tnear) Tnear = T1;
    if (T2 < Tfar) Tfar = T2;
    if (Tnear > Tfar || Tfar < 0)
    {
      result.exists = false;
      return result;
    }
  }

  // Y planes
  if (r.dir.y == 0)
  {
    if (r.orig.y < min.y || r.orig.y > max.y)
    {
      result.exists = false;
      return result;
    }
  }
  else
  {
    float T1 = (min.y - r.orig.y) / r.dir.y;
    float T2 = (max.y - r.orig.y) / r.dir.y;

    if (T1 > T2) std::swap(T1, T2);
    if (T1 > Tnear) Tnear = T1;
    if (T2 < Tfar) Tfar = T2;
    if (Tnear > Tfar || Tfar < 0)
    {
      result.exists = false;
      return result;
    }
  }

  // Z planes
  if (r.dir.z == 0)
  {
    if (r.orig.z < min.z || r.orig.z > max.z)
    {
      result.exists = false;
      return result;
    }
  }
  else
  {
    float T1 = (min.z - r.orig.z) / r.dir.z;
    float T2 = (max.z - r.orig.z) / r.dir.z;

    if (T1 > T2) std::swap(T1, T2);
    if (T1 > Tnear) Tnear = T1;
    if (T2 < Tfar) Tfar = T2;
    if (Tnear > Tfar || Tfar < 0)
    {
      result.exists = false;
      return result;
    }
  }

  if (Tnear < eps)
  {
    result.exists = false;
    return result;
  }

  result.exists = true;
  result.point = r.orig + Tnear * r.dir;
  if ((result.point.x - min.x) < eps)
    result.normal = vec3(-1, 0, 0);
  else if ((result.point.x - max.x) < eps)
    result.normal = vec3(1, 0, 0);
  else if ((result.point.y - min.y) < eps)
    result.normal = vec3(0, -1, 0);
  else if ((result.point.y - max.y) < eps)
    result.normal = vec3(0, 1, 0);
  else if ((result.point.z - min.z) < eps)
    result.normal = vec3(0, 0, -1);
  else if ((result.point.z - max.z) < eps)
    result.normal = vec3(0, 0, 1);

  return result;
}
