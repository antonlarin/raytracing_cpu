#ifndef TRACER_HPP
#define TRACER_HPP

#include <QImage>

#include "DataEntities.hpp"
#include "Objects.hpp"

class Tracer {
public:
  ~Tracer();
  Tracer(std::string filename);
  QImage formImage();
  void loadData();

private:
  const unsigned int MAX_TRACE_LEVEL = 8;
  const unsigned int MAX_OBJECTS_NUM = 10;
  const unsigned int ANTIALIASING_ZONE = 3;

  vec3 shootRay(Ray r, unsigned int level);

  Sphere *loadSphere(std::ifstream &in);
  Polygon *loadPolygon(std::ifstream& in);
  Box* loadBox(std::ifstream &in);
  void loadMaterialProperties(std::ifstream &in, TraceObject *o);
  bool shootShadowRay(Ray &r);

  std::string filename;
  TraceObject **scene;
  unsigned int numOfObjects;
  unsigned int frameSizeX, frameSizeY;
  Camera camera;
  LightSource **lightSources;
  unsigned int numOfLightSources;
  vec3 ambientColor;
};

#endif // TRACER_HPP
